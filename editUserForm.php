<?php
session_start();
	include("dbsetup.php");
	$usersID = $_GET['userID'];
	
	$usersSQL = "SELECT * FROM Accounts WHERE Users.userID = $userID;";
	
	$usersDS = mysql_query($usersSQL); // while loop are only use if there is more records
	$users = mysql_fetch_array($usersDS); // $varible = mysql_fetch_array($varibleID) this type of loop is use for single record
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Edit User</title>
</head>

<body>
<h1>Edit User</h1>

<form method="GET" action="editUserAction.php">
	<ul>
    <li>
        <input type="hidden" name="uID" value="<?php echo $users['userID'];?>"/> <!--never allow users to change ID to protect it by changing the type="text" into type="hidden"--->
    </li>
     <li>
		<label>First Name</label>
        <input type="text" name="fName" value="<?php echo $users['firstName'];?>"/> <!--"<?php echo $users['firstName'];?>" when this is in the value this setup helps put information before erasing the whole info -->
	 </li>
     <li>
     	<label>Last Name</label>
        <input type="text" name="lName" value="<?php echo $users['lastName'];?>"/>
     </li>
     <li>
     	<label>Email</label>
        <input type="text" name="email" value="<?php echo $users['email'];?>"/>
     </li>
     <li>
     	<label>Password</label>
        <input type="text" name="password" value="<?php echo $users['password'];?>"/>
     </li>
     <li>
     	<label>City</label>
        <input type="text" name="city" value="<?php echo $users['city'];?>"/>
     </li>
 	<li>
     	<label>Type</label>
        <input type="text" name="type" value="<?php echo $users['type'];?>"/>
     </li>
     <li>
        <input type="submit" name="Edit User"/>
     </li>
    </ul>

</form>

</body>
</html>